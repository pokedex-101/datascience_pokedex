# Pokedex 101 API Documentation

# **DataScience**

---
Notebooks Structure
---


- Obtaining data
- Clasiffy by force
- Unknown statistics calculation
- Legendary calculation


---
Data Structure
---

- General Data cleaned
- groups by pokemon
- probability of legendary
- Total Data





<div align="center"><a href="https://github.com/AndresCendales"><img alt="GitHub followers" src="https://img.shields.io/github/followers/AndresCendales?label=AndresCendales&style=social"></a><a href="https://twitter.com/AndresCendaless"><img alt="Twitter URL" src="https://img.shields.io/twitter/url?label=AndresCendaless&style=social&url=https%3A%2F%twitter.com%2AndresCendaless"></a></div>